var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM menu;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO menu(food, beverage, price) VALUES (?, ?, ?)';

    var queryData = [params.food, params.beverage, params.price];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getinfo = function(menu_id, callback) {
    var query = 'CALL menu_getinfo(?)';
    var queryData = [menu_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE menu SET food = ?, beverage = ?, price = ? WHERE menu_id = ?';

    var queryData = [params.food, params.beverage, params.price, params.menu_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(params, callback) {
    var query = 'DELETE FROM menu WHERE menu_id = ?';

    var queryData = [params.menu_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};