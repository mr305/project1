var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT city, state FROM (SELECT * FROM address1 a WHERE NOT EXISTS (SELECT * FROM event_address ea WHERE ea.address1_id = a.address1_id)) AS Location;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getinfo = function(callback) {
    var query = 'SELECT street, city FROM (SELECT a.street, a.city\n' +
        'FROM address1 a\n' +
        'INNER JOIN event_address ea ON a.address1_id = ea.address1_id\n' +
        'WHERE ea.address1_id IN (SELECT address1_id FROM event_address ea WHERE address1_id = 5)) AS street_city;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query = function(callback) {
    var query = 'SELECT street, city, state FROM (SELECT * FROM address1 WHERE address1_id IN (SELECT address1_id FROM event_address)) AS Location;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query1 = function(callback) {
    var query = 'SELECT vehicle FROM (SELECT * FROM parking p WHERE NOT EXISTS (SELECT parking_id FROM parking_info pi WHERE parking_id = p.parking_id)) AS Vehicle;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query2 = function(callback) {
    var query = 'SELECT  e.event_name, COUNT(ea.address1_id) AS Compare_AVG\n' +
        'FROM event_address ea\n' +
        'RIGHT JOIN event e ON ea.event_id = e.event_id\n' +
        'GROUP BY e.event_id\n' +
        'HAVING COUNT(ea.address1_id) > (SELECT AVG(Compare_AVG)\n' +
        '  FROM \n' +
        '    (\n' +
        '    SELECT  e.event_name, COUNT(ea.address1_id) AS Compare_AVG\n' +
        '\tFROM event_address ea\n' +
        '\tRIGHT JOIN event e ON ea.event_id = e.event_id\n' +
        '\tGROUP BY e.event_id) AS Average);';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query3 = function(callback) {
    var query = 'SELECT a.street, a.city, a.state\n' +
        'FROM address1 a\n' +
        'INNER JOIN event_address ea ON a.address1_id = ea.address1_id\n' +
        'GROUP BY a.address1_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query4 = function(callback) {
    var query = 'SELECT  e.event_name, COUNT(ea.address1_id) AS locations\n' +
        'FROM event_address ea\n' +
        'RIGHT JOIN event e ON ea.event_id = e.event_id\n' +
        'GROUP BY e.event_id\n' +
        'HAVING COUNT(ea.address1_id) > MAX(locations);\n';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query5 = function(callback) {
    var query = 'SELECT  e.event_name, COUNT(ea.address1_id) AS locations\n' +
        'FROM event_address ea\n' +
        'LEFT JOIN event e ON ea.event_id = e.event_id\n' +
        'GROUP BY e.event_id\n' +
        'ORDER BY e.event_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query6 = function(callback) {
    var query = 'SELECT city AS Cities_and_Events FROM address1\n' +
        'UNION \n' +
        'SELECT event_name FROM event';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query7 = function(callback) {
    var query = 'SELECT DISTINCT last_name FROM attendee;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query8 = function(callback) {
    var query = 'SELECT  e.event_name, COUNT(ea.address1_id) AS Count\n' +
        'FROM event_address ea\n' +
        'RIGHT JOIN event e ON ea.event_id = e.event_id\n' +
        'GROUP BY e.event_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.query9 = function(callback) {
    var query = 'SELECT AVG(Compare_AVG) AS Average\n' +
        '  FROM \n' +
        '    (\n' +
        '    SELECT  e.event_name, COUNT(ea.address1_id) AS Compare_AVG\n' +
        '\tFROM event_address ea\n' +
        '\tRIGHT JOIN event e ON ea.event_id = e.event_id\n' +
        '\tGROUP BY e.event_id) AS Average;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};