var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM event;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'CALL event_add(?) ';

    var queryData = [params.event_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.getinfo = function(event_id, callback) {
    var query = 'CALL event_getinfo(?)';
    var queryData = [event_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE event SET event_name = ? WHERE event_id = ?';

    var queryData = [params.event_name, params.event_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(params, callback) {
    var query = 'DELETE FROM event WHERE event_id = ?';

    var queryData = [params.event_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};