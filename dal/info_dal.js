var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM info;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO info(allowed, denied) VALUES (?, ?)';

    var queryData = [params.allowed, params.denied];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getinfo = function(info_id, callback) {
    var query = 'CALL info_getinfo(?)';
    var queryData = [info_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE info SET allowed = ?, denied = ? WHERE info_id = ?';

    var queryData = [params.allowed, params.denied, params.info_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(params, callback) {
    var query = 'DELETE FROM info WHERE info_id = ?';

    var queryData = [params.info_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};