var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM attendee;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO attendee(email, first_name, last_name) VALUES (?, ?, ?)';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getinfo = function(attendee_id, callback) {
    var query = 'CALL attendee_getinfo(?)';
    var queryData = [attendee_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE attendee SET email = ?, first_name = ?, last_name = ? WHERE attendee_id = ?';

    var queryData = [params.email, params.first_name, params.last_name, params.attendee_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(params, callback) {
    var query = 'DELETE FROM attendee WHERE attendee_id = ?';

    var queryData = [params.attendee_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};