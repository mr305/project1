var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM parking;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO parking(parking_num, vehicle) VALUES (?, ?)';

    var queryData = [params.parking_num, params.vehicle];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getinfo = function(parking_id, callback) {
    var query = 'CALL parking_getinfo(?)';
    var queryData = [parking_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE parking SET parking_num = ?, vehicle = ? WHERE parking_id = ?';

    var queryData = [params.parking_num, params.vehicle, params.parking_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(params, callback) {
    var query = 'DELETE FROM parking WHERE parking_id = ?';

    var queryData = [params.parking_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};