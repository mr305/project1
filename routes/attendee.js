var express = require('express');
var router = express.Router();
var attendee_dal = require('../dal/attendee_dal');

/* GET users listing. */
router.get('/all', function(req, res, next) {
    attendee_dal.getAll(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('attendee/attendee_view_all', {attendee: result,
                was_successful_insert: req.query.was_successful_insert, was_successful_delete: req.query.was_successful_delete,
                was_successful_update: req.query.was_successful_update});
        }
    })
});

router.get('/add', function(req, res) {
    res.render('attendee/attendee_add');
});

router.get('/insert', function(req, res) {
    attendee_dal.insert(req.query, function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            res.redirect(302, '/attendee/all' + "?&was_successful_insert=1");
        }
    });
});

router.get('/edit', function(req, res){
    attendee_dal.getinfo(req.query.attendee_id, function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('attendee/AttendeeUpdate',
                {attendee: result[0][0]}
            );
        }
    });
});

router.get('/update', function(req, res) {
    attendee_dal.update(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/attendee/all' + "?&was_successful_update=1");
        }
    });
});

router.get('/delete', function(req, res) {
    attendee_dal.delete(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/attendee/all' + "?&was_successful_delete=1");
        }
    });
});
module.exports = router;
