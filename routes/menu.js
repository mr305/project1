var express = require('express');
var router = express.Router();
var menu_dal = require('../dal/menu_dal');

/* GET users listing. */
router.get('/all', function(req, res, next) {
    menu_dal.getAll(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('menu/menu_view_all', {menu: result,
                was_successful_insert: req.query.was_successful_insert, was_successful_delete: req.query.was_successful_delete,
                was_successful_update: req.query.was_successful_update});
        }
    })
});

router.get('/add', function(req, res) {
    res.render('menu/menu_add');
});

router.get('/insert', function(req, res) {
    menu_dal.insert(req.query, function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            res.redirect(302, '/menu/all'  + "?&was_successful_insert=1");
        }
    });
});

router.get('/edit', function(req, res){
    menu_dal.getinfo(req.query.menu_id, function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('menu/MenuUpdate',
                {menu: result[0][0]}
            );
        }
    });
});

router.get('/update', function(req, res) {
    menu_dal.update(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/menu/all' + "?&was_successful_update=1");
        }
    });
});

router.get('/delete', function(req, res) {
    menu_dal.delete(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/menu/all' + "?&was_successful_delete=1");
        }
    });
});
module.exports = router;