var express = require('express');
var router = express.Router();
var SQL_dal = require('../dal/SQL_dal');

/* GET users listing. */
router.get('/all', function(req, res, next) {
    SQL_dal.getAll(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query1', {result: result});
        }
    })
});

router.get('/info', function(req, res, next) {
    SQL_dal.getinfo(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query2', {result: result});
        }
    })
});


router.get('/query', function(req, res, next) {
    SQL_dal.query(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query3', {result: result});
        }
    })
});

router.get('/query1', function(req, res, next) {
    SQL_dal.query1(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query4', {result: result});
        }
    })
});

router.get('/query2', function(req, res, next) {
    SQL_dal.query2(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query5', {result: result});
        }
    })
});

router.get('/query3', function(req, res, next) {
    SQL_dal.query3(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query6', {result: result});
        }
    })
});

router.get('/query4', function(req, res, next) {
    SQL_dal.query4(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query7', {result: result});
        }
    })
});

router.get('/query5', function(req, res, next) {
    SQL_dal.query5(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query8', {result: result});
        }
    })
});

router.get('/query6', function(req, res, next) {
    SQL_dal.query6(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query9', {result: result});
        }
    })
});

router.get('/query7', function(req, res, next) {
    SQL_dal.query7(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query10', {result: result});
        }
    })
});

router.get('/query8', function(req, res, next) {
    SQL_dal.query8(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query11', {result: result});
        }
    })
});



router.get('/query9', function(req, res, next) {
    SQL_dal.query9(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('SQL/SQL_query12', {result: result});
        }
    })
});

module.exports = router;