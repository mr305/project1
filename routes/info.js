var express = require('express');
var router = express.Router();
var info_dal = require('../dal/info_dal');

/* GET users listing. */
router.get('/all', function(req, res, next) {
    info_dal.getAll(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('info/info_view_all', {info: result,
                was_successful_insert: req.query.was_successful_insert, was_successful_delete: req.query.was_successful_delete,
                was_successful_update: req.query.was_successful_update});
        }
    })
});

router.get('/add', function(req, res) {
    res.render('info/info_add');
});

router.get('/insert', function(req, res) {
    info_dal.insert(req.query, function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            res.redirect(302, '/info/all' + "?&was_successful_insert=1");
        }
    });
});

router.get('/edit', function(req, res){
    info_dal.getinfo(req.query.info_id, function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('info/InfoUpdate',
                {info: result[0][0]}
            );
        }
    });
});

router.get('/update', function(req, res) {
    info_dal.update(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/info/all' + "?&was_successful_update=1");
        }
    });
});

router.get('/delete', function(req, res) {
    info_dal.delete(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/info/all' + "?&was_successful_delete=1");
        }
    });
});
module.exports = router;