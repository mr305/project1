var express = require('express');
var router = express.Router();
var parking_dal = require('../dal/parking_dal');

/* GET users listing. */
router.get('/all', function(req, res, next) {
    parking_dal.getAll(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('parking/parking_view_all', {parking: result,
                was_successful_insert: req.query.was_successful_insert, was_successful_delete: req.query.was_successful_delete,
                was_successful_update: req.query.was_successful_update});
        }
    })
});

router.get('/add', function(req, res) {
    res.render('parking/parking_add');
});

router.get('/insert', function(req, res) {
    parking_dal.insert(req.query, function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            res.redirect(302, '/parking/all' + "?&was_successful_insert=1");
        }
    });
});

router.get('/edit', function(req, res){
    parking_dal.getinfo(req.query.parking_id, function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('parking/ParkingUpdate',
                {parking: result[0][0]}
            );
        }
    });
});

router.get('/update', function(req, res) {
    parking_dal.update(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/parking/all' + "?&was_successful_update=1");
        }
    });
});

router.get('/delete', function(req, res) {
    parking_dal.delete(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/parking/all' + "?&was_successful_delete=1");
        }
    });
});
module.exports = router;