var express = require('express');
var router = express.Router();
var address1_dal = require('../dal/address1_dal');

/* GET users listing. */
router.get('/all', function(req, res, next) {
    address1_dal.getAll(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('address1/address1_view_all', {address1: result,
                was_successful_insert: req.query.was_successful_insert, was_successful_delete: req.query.was_successful_delete,
                was_successful_update: req.query.was_successful_update});
        }
    })
});

router.get('/add', function(req, res) {
    res.render('address1/address1_add');
});

router.get('/insert', function(req, res) {
    address1_dal.insert(req.query, function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            res.redirect(302, '/address1/all' + "?&was_successful_insert=1");
        }
    });
});

router.get('/edit', function(req, res){
    address1_dal.getinfo(req.query.address1_id, function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('address1/Address1Update',
                {address1: result[0][0]}
            );
        }
    });
});

router.get('/update', function(req, res) {
    address1_dal.update(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/address1/all' + "?&was_successful_update=1");
        }
    });
});

router.get('/delete', function(req, res) {
    address1_dal.delete(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/address1/all' + "?&was_successful_delete=1");
        }
    });
});
module.exports = router;
