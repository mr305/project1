var express = require('express');
var router = express.Router();
var event_dal = require('../dal/event_dal');

/* GET users listing. */
router.get('/all', function(req, res, next) {
    event_dal.getAll(function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(result);
            res.render('event/event_view_all', {event: result,
                was_successful_insert: req.query.was_successful_insert, was_successful_delete: req.query.was_successful_delete,
                was_successful_update: req.query.was_successful_update});
        }
    })
});

router.get('/add', function(req, res) {
    res.render('event/event_add');
});

router.get('/insert', function(req, res) {
    event_dal.insert(req.query, function(err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            res.redirect(302, '/event/all' + "?&was_successful_insert=1");
        }
    });
});


router.get('/edit', function(req, res){
    event_dal.getinfo(req.query.event_id, function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('event/EventUpdate',
                {event: result[0][0]}
            );
        }
    });
});

router.get('/update', function(req, res) {
    event_dal.update(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/event/all' + "?&was_successful_update=1");
        }
    });
});

router.get('/delete', function(req, res) {
    event_dal.delete(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.redirect(302, '/event/all' + "?&was_successful_delete=1");
        }
    });
});
module.exports = router;

